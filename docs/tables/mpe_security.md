---
layout: article
title: MPE_SECURITY
description: The MPE_SECURITY table determines which GROUPS can VIEW, EDIT, or APPROVE which tables or libraries
og_title: MPE_SECURITY Table Documentation
og_image: ../img/securitytable.png
---

# MPE_SECURITY

The MPE_SECURITY table determines which GROUPS can VIEW, EDIT, or APPROVE which tables or libraries

![Screenshot](../img/securitytable.png)

Edits to this table are covered by a (backend) post edit hook, which will automatically uppercase the LIBREF, DSN and ACCESS_LEVEL.


## Columns

 - 🔑 `TX_FROM num`: SCD2 open datetime
 - 🔑 `LIBREF char(8)`: The LIBREF of the target table
 - 🔑 `DSN char(32)`: The name of the target dataset (or format catalog)
 - 🔑 `ACCESS_LEVEL char(100)`: Either VIEW, EDIT or APPROVE
 - 🔑 `SAS_GROUP char(100)`: The SAS Group to which to provide access
 - `TX_TO num`: SCD2 close datetime