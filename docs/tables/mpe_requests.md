---
layout: article
title: MPE_REQUESTS
description: The MPE_REQUESTS table contains an entry with details for every backend request (user, timestamp, job name)
og_image: /img/mpe_requests.png
---

# MPE_REQUESTS
The MPE_REQUESTS table contains an entry with details for every backend request.  This is applied in the [mpeterm.sas](https://code.datacontroller.io/mpeterm_8sas.html) macro.

In some scenarios this can cause table locks.  To avoid this, you can set the [dc_request_logs](/dcc-options/#dc_request_logs) option to NO.

![](/img/mpe_requests.png)

## Columns

There is explicitly **no primary key** on this (transaction) table for the purpose of speedy updates

 - `REQUEST_DTTM num`: Timestamp of the request
 - `REQUEST_USER char(64)`: The user making the request
 - `REQUEST_SERVICE char(64)`: The STP/Job being executed (under $apploc/services)
 - `REQUEST_PARAMS char(128)`: Empty for now

