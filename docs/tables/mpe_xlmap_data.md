---
layout: article
title: MPE_XLMAP_DATA
description: The MPE_XLMAP_DATA table stores the data from complex excel uploads in a vertical format.
og_title: MPE_XLMAP_DATA Table Documentation
og_image: ../img/mpe_xlmap_data.png
---

# MPE_XLMAP_DATA

The MPE_XLMAP_DATA table stores the data from complex excel uploads in a vertical format.  It is possible to configure a different target, so long as it has the same attributes, in the [MPE_XLMAP_INFO](/tables/mpe_xlmap_info) table.

![Screenshot](../img/mpe_xlmap_data.png)

See also:

* [Excel Uploads](/excel)
* [MPE_XLMAP_INFO](/tables/mpe_xlmap_info)
* [MPE_XLMAP_RULES](/tables/mpe_xlmap_rules)

!!!note
    This table cannot be edited directly!  Any target tables configured in [MPE_XLMAP_INFO](/tables/mpe_xlmap_info) are also removed from the Tables list in the LOAD menu.

## Columns

 - 🔑 `LOAD_REF char(32)`:  The Unique Load reference.  Applied automatically.
 - 🔑 `XLMAP_ID char(32)`:  The unique, UPPERCASE reference for the excel map.  Applied automatically.
 - 🔑 `XLMAP_RANGE_ID char(32)`: A unique reference for the specific range being loaded.  Applied automatically.
 - `ROW_NO num`: For single-cell / single-row sources, this is always 1.  Otherwise, it is the row number of the rectangular range being extracted in this XLMAP_RANGE_ID.
 - `COL_NO num`: For single-cell / single-column sources, this is always 1.  Otherwise, it is the column number of the rectangular range being extracted in this XLMAP_RANGE_ID.
 - `VALUE_TXT char(4000)`: The value of the cell, in text format.