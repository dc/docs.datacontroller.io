---
layout: article
title: MPE_XLMAP_RULES
description: The MPE_XLMAP_RULES table contains the rules for mapping excel cells and ranges to XLMAP_IDs for upload into SAS
og_title: MPE_XLMAP_RULES Table Documentation
og_image: ../img/mpe_xlmap_rules.png
---

# MPE_XLMAP_RULES

The MPE_XLMAP_RULES table contains the rules for mapping excel cells and ranges to XLMAP_IDs for [upload into SAS](/excel)

![Screenshot](../img/mpe_xlmap_rules.png)

See also:

* [Excel Uploads](/excel)
* [MPE_XLMAP_DATA](/tables/mpe_xlmap_data)
* [MPE_XLMAP_INFO](/tables/mpe_xlmap_info)


## Columns

 - `TX_FROM num`: SCD2 open datetime
 - 🔑 `TX_TO num`: SCD2 close datetime
 - 🔑 `XLMAP_ID char(32)`:  A unique, UPPERCASE reference for the excel map.
 - 🔑 `XLMAP_RANGE_ID char(32)`: A unique reference for the specific range being loaded
 - `XLMAP_SHEET char(32)`:  The sheet name in which to capture the data. Examples:
    - `Sheet2` - an absolute reference
    - `/1` - the first tab in the workbook
 - `XLMAP_START char(1000)`:  The rule used to find the top left of the range. Use "R1C1" notation to move the target. Examples:
    - `ABSOLUTE F4` - an absolute reference
    - `RELATIVE R[2]C[2]` - In the XMLMAP_START case, this is the same as `ABSOLUTE B2`
    - `MATCH P R[0]C[2]: My Test` - search column P for the string "My Test" then move 2 columns right
    - `MATCH 7 R[-2]C[-1]: Top Banana` - search row 7 for the string "Top Banana" then move 2 rows up and 1 column left
 - `XLMAP_FINISH char(1000)`: The rule used to find the end of the range. Leave blank for individual cells. Example values include those listed under XLMAP_START, plus:
    - `BLANKROW` - search down (from XLMAP_START) until an entirely blank row is found, then choose the row above it
    - `LASTDOWN` - The last non blank cell below the XLMAP_START cell.  In the `RELATIVE R[x]C[x]` case, this is offset from from XLMAP_START rather than A1
