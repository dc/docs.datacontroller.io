---
layout: article
title: MPE_XLMAP_INFO
description: The MPE_XLMAP_INFO table provides information about a particular XLMAP_ID
og_title: MPE_XLMAP_INFO Table Documentation
og_image: ../img/mpe_xlmap_info.png
---

# MPE_XLMAP_INFO

The MPE_XLMAP_INFO table provides information about a particular XLMAP_ID

The information is optional (unless you wish to configure a non-default target table).

![Screenshot](../img/mpe_xlmap_info.png)

See also:

* [Excel Uploads](/excel)
* [MPE_XLMAP_DATA](/tables/mpe_xlmap_data)
* [MPE_XLMAP_RULES](/tables/mpe_xlmap_rules)




## Columns

 - `TX_FROM num`: SCD2 open datetime
 - 🔑 `TX_TO num`: SCD2 close datetime
 - 🔑 `XLMAP_ID char(32)`:  A unique, UPPERCASE reference for the excel map.
 - `XLMAP_DESCRIPTION char(1000)`:  Map Description
 - `XLMAP_TARGETLIBDS char(41)`: An alternative target table to which to upload the data.  This MUST have the same structure as the [MPE_XLMAP_DATA](/tables/mpe_xlmap_data) table.  The table must also be configured in [MPE_TABLES](/tables/mpe_tables).