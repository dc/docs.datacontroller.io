---
layout: article
title: Data Restore
description: How to restore a previous version of a Data Controller table
og_image:  https://docs.datacontroller.io/img/restore.png
---

# Data Restore

For those tables which have [Audit Tracking](/dcc-tables/#audit_libds) enabled, it is possible to restore the data to an earlier state!

Simply open the submit to be reverted (via HISTORY or the table INFO/VERSIONS screen), and click the red **REVERT** button.  This will generate a NEW submission, containing the necessary reversal entries.  This new submission **must then be approved** in the usual fashion.

![](/img/restore.png)

This approach means that the audit history remains intact - there is simply a new entry, which reverts all the previous entries.

## Caveats

Note that there are some caveats to this feature:

- User must have EDIT permission
- Table must have TXTEMPORAL or UPDATE Load Type
- Changes **outside** of Data Controller cannot be reversed
- If there are COLUMN or ROW level security rules, the restore will abort
- If the model has changed (new / deleted) columns the restore will abort

## Technical Information

The restore works by undoing all the changes listed in the [MPE_AUDIT](/tables/mpe_audit/) table.  The keys from this table (since and including the version to be restored) are left joined to the base table (to get current values) to create a staging dataset, and then the changes applied in reverse chronological order using [this macro](https://core.sasjs.io/mp__stripdiffs_8sas.html).  This staging dataset is then submitted for approval, providing a final sense check before the new / reverted state is applied. 

Source code for the restore process is available [here](https://git.datacontroller.io/dc/dc/src/branch/main/sas/sasjs/services/editors/restore.sas).